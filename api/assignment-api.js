import axios from 'axios'
import moment from 'moment'

const baseUrl = 'http://localhost:5000/'
const resourceUrl = 'assignments'

export default class AssignmentApi {
  static getAll = async () => {
    try {
      // TODO: get all assignments by provider id
      const response = await axios.get(`${baseUrl}${resourceUrl}`)
      return response.data.map(a => toEvent(a))
    } catch (error) {
      throw error
    }
  }

  static getByTime = async ( start, end, timezone ) => {
    try {
      // TODO: this call should be a post to get something by a time frame on the server
      const allAssignments = await AssignmentApi.getAll()
      const assignments = allAssignments.filter(assignment => 
        moment(assignment.start).isBetween(start, end, 'day', '[]') ||
        moment(assignment.end).isBetween(start, end, 'day', '[]')
      )
      return assignments
    } catch (error) {
      throw error
    }
  }
}

const toEvent = assignment => {
  const { startTime, duration, worksite, ...rest } = assignment;

  const dur = parseInt(duration)
  const start = moment(startTime).format()
  const end = moment(start).add(dur, 'hours').format()

  return {
    title: `${worksite}`,
    overlap: true,
    start,
    end,
    duration,
    worksite,
   ...rest
  }
}