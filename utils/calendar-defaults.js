import moment from 'moment'

const popoverTemplate = `
  <div class="popover" role="tooltip">
    <div class="arrow"></div>
    <h3 class="popover-header"></h3>
    <div class="popover-body"></div>
  </div>
`

const popoverBodyTemplate = ({ start, end, status, type }) => { 
  const dateTemplate = getDateTemplate(start, end)
  const statusTemplate = getStatusTemplate(status)
  const typeTemplate = getTypeTemplate(type)

  return `
    ${dateTemplate}
    ${statusTemplate}
    ${typeTemplate}
  `
}

const getDateTemplate = (start, end) => {
  const dateFormat = 'dddd, MMMM D, YYYY'
  const timeFormat = 'h:mm a'

  const startDate = moment(start)
  const endDate = moment(end)

  const differenceInDays = moment(moment(end).format('YYYY-MM-DD')).diff(moment(moment(start).format('YYYY-MM-DD')), 'days')
  let dateTemplate = ''

  if (differenceInDays > 0) {
    dateTemplate = `
      <div>
        <span>${startDate.format(dateFormat)}</span>
        <span>${startDate.format(timeFormat)}</span>
      </div>
      <div>
        <span>${endDate.format(dateFormat)}</span>
        <span>${endDate.format(timeFormat)}</span>
      </div>
    `
  } else {
    dateTemplate = `
      <div>
        <span>${startDate.format(dateFormat)}</span>
        <div>
          <span>${startDate.format(timeFormat)}</span>
          <span>to</span>
          <span>${endDate.format(timeFormat)}</span>
        </div>
      </div>
    `
  }

  return `${dateTemplate}`
}

const getStatusTemplate = status => {
  return `
    <div>
      <span>Status</span>
      <span>:</span>
      <span>${status}</span>
    </div>
  `
}

const getTypeTemplate = type => {
  return `
    <div>
      <span>Type</span>
      <span>:</span>
      <span>${type}</span>
    </div>
  `
}

export const getColumnHeaderText = moment => {
  let weekdayAbrv = ''
  const dayOfWeek = moment.isoWeekday()

  if (dayOfWeek === 1) {
    weekdayAbrv = 'M'
  } else if (dayOfWeek === 2 || dayOfWeek === 4) {
    weekdayAbrv = 'T'
  } else if (dayOfWeek === 3) {
    weekdayAbrv = 'W'
  } else if (dayOfWeek === 5) {
    weekdayAbrv = 'F'
  } else if (dayOfWeek === 6 || dayOfWeek === 7) {
    weekdayAbrv = 'S'
  }

  return weekdayAbrv
}
export const defaultView = 'month'
export const defaultHeight = 300
export const defaultContentHeight = 300
export const defaultTitleFormat = 'MMM YYYY'

export const defaultEventRender = (eventObj, $el ) => {
  $el.popover({
    container: 'body',
    content: popoverBodyTemplate(eventObj),
    html: true,
    placement: 'bottom',
    template: popoverTemplate,
    title: eventObj.worksite,
    trigger: 'hover',
  })
}

export default {
  contentHeight: defaultContentHeight,
  columnHeaderText: moment => getColumnHeaderText(moment),
  defaultView,
  eventBorderColor: '#4A90E2',
  eventColor: '#4A90E2',
  eventTextColor: '#fff',
  eventRender: defaultEventRender,
  height: defaultHeight,
  showNonCurrentDates: false,
  titleFormat: defaultTitleFormat
}