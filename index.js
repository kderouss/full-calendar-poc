import './site-styles.css'
import 'fullcalendar/dist/fullcalendar.min.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@babel/polyfill'
import 'jquery'
import 'popper.js'
import 'bootstrap/js/dist/popover'
import moment from 'moment'
import 'fullcalendar'
import assignmentApi from './api/assignment-api'
import calendarDefault from './utils/calendar-defaults';
import AssignmentApi from './api/assignment-api';

const today = moment()
const nextMonth = today.clone().add(1, 'month').date(1).hour(1).minutes(0).seconds(0)
const followingMonth = today.clone().add(2, 'month').date(1).hour(1).minutes(0).seconds(0)

const calendarIds = [ '#calendar1', '#calendar2', '#calendar3' ]

const stringCompare = (strA, strB) => {
  const s1 = strA.toLowerCase()
  const s2 = strB.toLowerCase()

  if (s1 < s2) {
    return -1
  }

  if (s1 > s2) {
    return 1
  }

  return 0
}

const convertTime12to24 = timeIn12 => {
  const [time, modifier] = timeIn12.split(' ')
  let [hours, minutes] = time.split(':')

  if (hours === '12') {
    hours = '00'
  }

  if (modifier.toLowerCase() === 'am' && hours.length === 1) {
    hours = `0${hours}`
  }

  if( modifier.toLowerCase() === 'pm') {
    hours = parseInt(hours, 10) + 12
  }

  return `${hours}:${minutes}`
}

const listViewSortStatus = {
  date: { 
    asc: true,
    sort: (dateString1, dateString2) => {
      const date1 = moment(dateString1)

      if (date1.isBefore(dateString2, 'day')) {
        return -1;
      }
      
      if (date1.isAfter(dateString2, 'day')) {
        return 1;
      }

      if (date1.isSame(dateString2, 'day')) {
        return 0;
      }
    }
  },
  startTime: { 
    asc: false,
    sort: (timeString1, timeString2) => {
      const today = moment().format('YYYY-MM-DD')

      timeString1 = convertTime12to24(timeString1)
      timeString2 = convertTime12to24(timeString2)

      timeString1 = `${today}T${timeString1}`
      timeString2 = `${today}T${timeString2}`

      const time1 = moment(timeString1, 'HH:mm')

      if (time1.isBefore(timeString2, 'second')) {
        return -1;
      }

      if (time1.isAfter(timeString2, 'second')) {
        return 1;
      }

      if (time1.isSame(timeString2, 'second')) {
        return 0;
      }
    }
  },
  duration: { 
    asc: false,
    sort: (durString1, durString2) => {
      const dur1 = durString1.replace(' Hours', '')
      const dur2 = durString2.replace(' Hours', '')

      return parseInt(dur1) - parseInt(dur2)
    }
  },
  worksite: { 
    asc: false,
    sort: stringCompare
  },
  status: { 
    asc: false,
    sort: stringCompare
  },
  type: { 
    asc: false,
    sort: stringCompare
  }
}

$('#calendar1').fullCalendar({
  ...calendarDefault,
  customButtons: {
    customPrevButton: {
      text: 'Previous',
      click: () => {
        calendarIds.forEach(id => $(id).fullCalendar('incrementDate', { month: -1 }))
      },
      icon: 'left-single-arrow'
    }
  },
  events: (start, end, timezone, callback) => {
    assignmentApi.getByTime(start, end, timezone).then(assignments => callback(assignments))
  },
  header: {
    left: 'customPrevButton',
    center: 'title',
    right: ''
  }
})

$('#calendar2').fullCalendar({
  ...calendarDefault,
  defaultDate: nextMonth,
  events: (start, end, timezone, callback) => {
    assignmentApi.getByTime(start, end, timezone).then(assignments => callback(assignments))
  },
  header: {
    left: '',
    center: 'title',
    right: ''
  }
})

$('#calendar3').fullCalendar({
  ...calendarDefault,
  customButtons: {
    customNextButton: {
      text: 'Next',
      click: () => {
        calendarIds.forEach(id => $(id).fullCalendar('incrementDate', { month: 1 }))
      },
      icon: 'right-single-arrow'
    }
  },
  defaultDate: followingMonth,
  events: (start, end, timezone, callback) => {
    assignmentApi.getByTime(start, end, timezone).then(assignments => callback(assignments))
  },
  header: {
    left: '',
    center: 'title',
    right: 'customNextButton'
  }
})

$(window).on('load', () => {
  AssignmentApi
    .getAll()
    .then(assignments => 
      assignments.forEach( a => $('.calendar-list-view .body').append(createListViewRow(a)))
    )
})

$('.calendar-list-view .header .row').click(event => {
  const col = event.target.id
  listViewSortStatus[col].asc = !listViewSortStatus[col].asc

  const sortedRows = $('.calendar-list-view .body .row').sort((rowA, rowB) => {
    rowA = $(rowA)
    rowB = $(rowB)

    const idA = rowA.attr('id')
    const colA = rowA.find(`#${idA}-${col}`)
    const dataA = colA.text()

    const idB = rowB.attr('id')
    const colB = rowB.find(`#${idB}-${col}`)
    const dataB = colB.text()

    return listViewSortStatus[col].asc 
      ? listViewSortStatus[col].sort(dataA, dataB)
      : listViewSortStatus[col].sort(dataA, dataB) * -1
  })

  $('.calendar-list-view .body').html('')
  sortedRows.each((index, row) => $('.calendar-list-view .body').append($(row)[0].outerHTML))
})

const createListViewRow = assignment => {
  const { id, duration, worksite, status, type } = assignment
  const monthFormat = 'MM/DD/YYYY'
  const timeFormat = 'h:mm a'

  const start = moment(assignment.start).format(monthFormat)
  const time = moment(assignment.start).format(timeFormat)

  return `
    <div id="${id}" class="row">
      <div id="${id}-date" class="col-md">${start}</div>
      <div id="${id}-startTime" class="col-md">${time}</div>
      <div id="${id}-duration" class="col-md">${duration} Hours</div>
      <div id="${id}-worksite" class="col-md">${worksite}</div>
      <div id="${id}-status" class="col-md">${status}</div>
      <div id="${id}-type" class="col-md">${type}</div>
    </div>
`
}